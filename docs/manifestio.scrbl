#lang scribble/manual

@title{The Spritely Manifestio / Design Doc}
@author{Christopher Lemmer Webber}

@section{What is Spritely?}

Currently?  Vaporware! Over time?
Hopefully a pretty interesting approach to distributed social networks:

@itemlist[
  @item{A distributed social network following the
        @hyperlink["https://www.w3.org/TR/activitypub/"]{ActivityPub}
        protocol}
  @item{Written in @hyperlink["https://racket-lang.org/"]{Racket}}
  @item{100% Free Software, mostly under the GPL, with some libraries under
        the LGPL}
  @item{Goal of being easily packaged in major software distributions from
        the start (Debian, GuixSD, Fedora, etc)}
  @item{Good distributed systems foundations
        (follows the @hyperlink["https://en.wikipedia.org/wiki/Actor_model"]{actor model})
        built on composable, secure interactions
        (using @hyperlink["https://en.wikipedia.org/wiki/Capability-based_security"]{object capability security})}
  @item{An emphasis on an inclusive and friendly development community}]

Some slightly more distant goals of Spritely is to support:

@itemlist[
  @item{Distributed social games
        (see @hyperlink["https://dustycloud.org/misc/mmose.pdf"]{Massively Multiplayer Online Secure Environments})}
  @item{Peer to peer interactions over the
        @hyperlink["https://www.torproject.org/docs/onion-services"]{Tor Onion Protocol}
        or something like it.
        Want to run a local game for your friends without suffering outside harassment?
        Spin it up locally and securely!}
  @item{An ActivityPub client which ships out of the box which supports all the above
        as well as a petnames based database to reduce phishing and support
        decentralized global naming systems.}]

Why the name spritely?
It's apparently an archaic version of the term "sprightly",
which I feel is appropriate.
In addition, Spritely aims to make some inroads for bringing distributed
game systems to the federated social web.
Plus, this opens up the possibility to have some cute fairy-like creatures
for mascots (as 16-bit-era game sprites), an idea which I like.


@section{On being a distributed social network}

I feel like the reasons to have a decentralized/distributed/federated
social network today are self-describing.
Surveillance, worldwide censorship, the inability for marginalized
communities to be able to self-represent and self-moderate, the
troubling nature of monopolistic purview of our daily lives, etc etc.

You probably already agree.
If you don't... do any amount of reading on the terrifying state of
centralized social networks and come back. :)

@section{On the decision to build on top of ActivityPub}

The W3C @hyperlink["https://www.w3.org/TR/activitypub/"]{ActivityPub}
protocol was developed by studying usage behaviors across many social
networks and in conversations with many existing decentralized social
networks.
(The author is also biased in their appreciation for ActivityPub by
being co-editor of the standard.)
The system is fairly simple where many actors send messages to each
others' inboxes and which can read to each others' outboxes.
ActivityPub provides both a simple client-to-server API and a
server-to-server API, and Spritely will implement both.
This means that Spritely will be able to integrate with
@hyperlink["https://activitypub.rocks/implementation-report/"]{existing federated social networks}
as part of the growing ActivityPub network, and any client written
for a client-to-server compliant ActivityPub application should
also work with Spritely.

Spritely has some more ambitious goals as well, and thus Spritely
intends to ship both a server and a client which are backwards
compatible with existing ActivityPub servers while providing some new
features, such as peer to peer connections and distributed social
games.
More on these below.

@section{Why Racket?}

@section{On the actor model and ocap}

@section{Distributed identifiers, etc}

@section{Licensing decisions}

@section{On more ambitious social games}

When it comes to online social spaces, in the 1980s and 1990s, the
internet was a much more cyberpunk space than it is today.
MUDs, MOOs, etc had not only 

@section{On peer to peer servers}


@section{On the relationship to MediaGoblin}

@section{Milestones}
